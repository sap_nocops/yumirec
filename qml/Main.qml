/*
 * Copyright (C) 2021  Lorenzo Torracchi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * yumirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtSystemInfo 5.0
import GSettings 1.0

import Example 1.0

MainView {
    id: mainView
    objectName: 'mainView'
    applicationName: 'yumirec.sap'
    automaticOrientation: true
    property bool appSuspensionDisabled: false

    width: units.gu(45)
    height: units.gu(75)

    Page {
        id: recordingPage
        anchors.fill: parent
        property bool recording: false

        header: PageHeader {
            id: header
            title: i18n.tr('Yumi Recorder')
        }

        ColumnLayout {
            spacing: units.gu(2)
            anchors {
                margins: units.gu(2)
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Item {
                Layout.fillHeight: true
            }

            Label {
                id: label
                Layout.alignment: Qt.AlignHCenter
                text: i18n.tr('Press the button to start/stop recording the screen!')
            }

            Button {
                id: recButton
                Layout.alignment: Qt.AlignHCenter
                text: i18n.tr('Record')
                visible: !recordingPage.recording
                onClicked: {
                    unsetAppSuspension();
                    Example.startRecording();
                    recordingPage.recording = true;
                }
            }

            Button {
                id: stopButton
                Layout.alignment: Qt.AlignHCenter
                text: i18n.tr('Stop')
                visible: recordingPage.recording
                onClicked: {
                    Example.stopRecording();
                    setAppSuspension();
                    recordingPage.recording = false;
                }
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }

    function checkAppSuspension() {
        var appidList = gsettings.lifecycleExemptAppids
        var appId = Qt.application.name
        var found = false

        if (appidList) {
            for(var i = 0; i < appidList.length; i++){
                if (appidList[i] == appId) {
                    found = true
                }
            }
        }

        return found
    }

    function setAppSuspension() {
        if (!mainView.checkAppSuspension()) {
            var appidList = gsettings.lifecycleExemptAppids
            var appId = Qt.application.name

            var newList = appidList.slice()
            newList.push(appId)
            console.log("Original app suspension list: " + appidList)
            console.log("Set app suspension: " + newList)
            gsettings.lifecycleExemptAppids = newList
        }
    }

    function unsetAppSuspension() {
        if (mainView.checkAppSuspension()) {
            var appidList = gsettings.lifecycleExemptAppids
            var appId = Qt.application.name
            var index = appidList.indexOf(appId);

            console.log("Original app suspension list: " + appidList)

            if (index > -1) {
              appidList.splice(index, 1);
            }
            console.log("Unset app suspension: " + appidList)
            gsettings.lifecycleExemptAppids = appidList
        }
    }

    GSettings {
        id: gsettings
        schema.id: "com.canonical.qtmir"

        Component.onCompleted: {
            mainView.appSuspensionDisabled = mainView.checkAppSuspension()
        }

        onValueChanged: {
            if (key == "lifecycleExemptAppids") {
                mainView.appSuspensionDisabled = mainView.checkAppSuspension()
            }
        }
    }

    ScreenSaver {
        screenSaverEnabled: !recordingPage.recording
    }
}
