/*
 * Copyright (C) 2021  Lorenzo Torracchi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * yumirecis distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QStandardPaths>
#include <QFile>
#include <QDir>

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("yumirec.sap");

    qDebug() << "Starting app from main.cpp";
    QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    qDebug() << "output path: " << path;
    if (!QFile::exists(path)) {
        QDir dir;
        bool createOk = dir.mkpath(path);
        if (!createOk) {
            qWarning() << "Unable to create recordings directory" << path;
            return 1;
        }
    }

    QQuickView *view = new QQuickView();
    view->setSource(QUrl("qrc:/Main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
