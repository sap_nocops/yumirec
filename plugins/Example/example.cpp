/*
 * Copyright (C) 2021  Lorenzo Torracchi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * yumirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QString>
#include <QStringList>
#include <QScreen>
#include <QDateTime>
#include <QGuiApplication>
#include <QStandardPaths>

#include "example.h"

Example::Example() {
    
}

void Example::startRecording() {
    QDateTime currentDateTime = QDateTime::currentDateTime();
    QString currentDate = currentDateTime.toString("yyyy-MM-dd-hh_mm_ss");
    QString fileName = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + currentDate + ".mp4";

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    int height = screenGeometry.height();
    int width = screenGeometry.width();

    QString fps = "fps=60:w=" + QString::number(width) + ":h=" + QString::number(width) + ":format=rgba";
    //mirscreencast -m /run/mir_socket --stdout --cap-interval 1 -s 384 640 | 
    QStringList mircastArguments;
    mircastArguments << "-m" << "/run/mir_socket" << "--stdout" << "--cap-interval" << "1" << QString::number(width) << QString::number(height);
    //mencoder -demuxer rawvideo -rawvideo fps=60:w=384:h=640:format=rgba -ovc x264 -o out.avi -
    QStringList mencoderArguments;
    mencoderArguments << "-demuxer" << "rawvideo" << "-rawvideo" << fps << "-ovc" << "x264" << "-o" << fileName + ".mp4" << "-";
    qDebug() << "mircast args: " << mircastArguments;
    qDebug() << "mencoder args: " << mencoderArguments;

    m_mirscreencastProcess.setStandardOutputProcess(&m_mencoderProcess);
    m_mirscreencastProcess.start("mirscreencast", mircastArguments);
    m_mencoderProcess.start("mencoder", mencoderArguments);
    m_mencoderProcess.setProcessChannelMode(QProcess::ForwardedChannels);
    if(!m_mirscreencastProcess.waitForStarted()) {
        qDebug() << "mircast std out: " << m_mirscreencastProcess.readAllStandardOutput();
        qDebug() << "mircast std error: " << m_mirscreencastProcess.readAllStandardError();
        qDebug() << "mircast error: " << m_mirscreencastProcess.error();
        m_mencoderProcess.terminate();
        return;
    }
    if(!m_mencoderProcess.waitForStarted()) {
        qDebug() << "mencoder std out: " << m_mencoderProcess.readAllStandardOutput();
        qDebug() << "mencoder std error: " << m_mencoderProcess.readAllStandardError();
        qDebug() << "mencoder error: " << m_mencoderProcess.error();
        m_mirscreencastProcess.terminate();
        return;
    }
}

void Example::stopRecording() {
    m_mirscreencastProcess.terminate();
    m_mirscreencastProcess.waitForFinished();
    m_mencoderProcess.terminate();
    m_mirscreencastProcess.waitForFinished();
}
